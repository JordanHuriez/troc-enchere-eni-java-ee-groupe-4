<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<header class=" bg-dark header-demodule fixed-top">
	<div class="container text-left text-white">
		<h1>Troc Enchere</h1>
	</div>
	<div class="container text-right text-white margin-top">
		<c:choose>
		
			<c:when test="${!empty sessionScope.userInSession}">
				<ul class="nav justify-content-end">
					<li class="nav-item"><a class="nav-link" href="#">Enchères</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Vendre
							un article</a></li>
					<li class="nav-item"><a class="nav-link" href="./afficherprofil">Mon
							profil</a></li>
					<li class="nav-item"><a class="nav-link" href="./index">Déconnexion</a></li>
				</ul>
			</c:when>
			<c:otherwise>
			<ul class="nav justify-content-end">
				<li class="nav-item"><a class="nav-item" href="./connection">S'inscrire - Se connecter</a></li>
				</ul>
				</c:otherwise>
		</c:choose>
	</div>

</header>