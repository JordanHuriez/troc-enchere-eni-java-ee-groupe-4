<%@page import="fr.eni.projet.bo.Article"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>

<jsp:include page="/WEB-INF/fragments/head.jsp"></jsp:include>

<body class="container">

	<%@ include file="./fragments/header.jsp"%>

	<div class="col-12">
		<h1 class="my-5 text-center">Listes des enchères</h1>

		<div class="row">
			<div class="mx-auto col-lg-6 col-md-6 col-sm-6 portfolio-item">
				<div class="card h-100">
					<div class="card-body cbconnected">
						<h6>Filtres :</h6>
						<form action="${targetURL}" method="post">
							<div class="form-group">
								<input name="recherche" type="text"
									class="btn btn-primary btn-lg btn-block"></input>
								<button name="submit" type="submit"
									class="btn btn-primary btn-lg btn-block">Rechercher</button>
							</div>
							<h6>Categorie :</h6>
							<select name="categorie" class="form-select"
								aria-label="Default select example">
								<option selected>Toutes</option>
								<option value="Informatique">Informatique</option>
								<option value="Ameublement">Ameublement</option>
								<option value="Vêtement">Vêtement</option>
								<option value="Sport&Loisirs">Sport&Loisirs</option>
							</select>
						</form>
					</div>
				</div>


			<div class="col-12">
				<h2 class="my-5 text-center">Articles à vendre</h2>
				<div class="row">
					<c:choose>
						<c:when test="${lstArticles.size()>0}">

							<c:forEach var="article" items="${lstArticles}">
								<ol class="listevente">
									<li class="liarticle"><c:out value="${article.nomArticle}" /></li>
									<li class="liarticle"><c:out
											value="${article.prixInitial}" />
									<li class="liarticle"><c:out
											value="${article.dateFinEncheres}" /></li>
									<li class="liarticle"><c:out
											value="${article.utilisateur.pseudo}" /></li>
								</ol>
							</c:forEach>

						</c:when>
						<c:otherwise>
							<p>Pas d'articles actuellement.</p>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
	</div>

	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

</html>