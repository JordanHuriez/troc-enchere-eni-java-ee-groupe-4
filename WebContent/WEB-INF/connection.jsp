<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<jsp:include page="/WEB-INF/fragments/head.jsp"></jsp:include>

<body>

	<!-- Page Content -->
	<div class="container">
	
		<%@ include file="./fragments/header.jsp"%>
		<br>
		<br>
		
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 portfolio-item">
				<div class="card h-100">
					<div class="card-body">
						<form method="post" action="./connection">
							<div class="form-group">
								<label for="login">Identifiant : </label> <input class="form-control"
									id="login" required name="login">
							</div>
							<div class="form-group">
								<label for="password">Mot de passe : </label> <input type="password"
									class="form-control" id="password" required name="password">
							</div>
							<div class="form-group">
								<input type="submit" value="Connexion" class="btn btn-primary">
							</div>
						</form>
						<a href="">Mot de passe oublié</a>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value=""
								id="flexCheckChecked" checked> <label
								class="form-check-label" for="flexCheckChecked"> Se souvenir de moi </label>
						</div>
						<div class="list-group  my-1">
							<a
								class="list-group-item list-group-item-action list-group-item-primary"
								href="<%=request.getContextPath()%>/creationProfil">Créer un compte</a>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->

		</div>
		<!-- /.container -->
</body>
</html>