<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/fragments/head.jsp"></jsp:include>
<head>
<meta charset="UTF-8">
<title>Mon Profil</title>
</head>
<body>
	<%@ include file="./fragments/header.jsp"%>

	<div class="container1">
		<h1 class="titre1">Formulaires</h1>
		<form class="form1" method="post"
			action="<%=request.getContextPath()%>/creationProfil">
			<div class="row mb-3">
				<div class="col">
					<input type="text" required class="form-control" placeholder="Pseudo" name="pseudo" minlength="3" maxlength="30">
				</div>
				<div class="col">
					<input type="text" required class="form-control" placeholder="Nom" name="nom" pattern="[^0-9]{3,30}">
				</div>
			</div>
			<div class="row mb-3">
				<div class="col">
					<input type="text" required class="form-control" placeholder="Prenom" name="prenom" pattern="[^0-9]{3,30}">
				</div>
				<div class="col">
					<input type="text" required class="form-control" placeholder="Email" name="email" maxlength="50">
				</div>
			</div>
			<div class="row mb-3">
				<div class="col">
					<input type="text" class="form-control" placeholder="Telephone" name="telephone" pattern="[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}">
				</div>
				<div class="col">
					<input type="text" required class="form-control" placeholder="Rue" name="rue">
				</div>
			</div>
			<div class="row mb-3">
				<div class="col">
					<input type="text" required class="form-control" placeholder="Code Postal" name="codePostal" pattern="[0-9]{5}">
				</div>
				<div class="col">
					<input type="text" required class="form-control" placeholder="Ville" name="ville">
				</div>
			</div>
			<div class="row mb-3">
				<div class="col">
					<input type="password" required class="form-control" placeholder="Mot De Passe" name="motDePasse" maxlength="30">
				</div>
				<div class="col">
					<input type="password" required class="form-control" placeholder="Confirmation" name="confirmation" maxlength="30">
				</div>
			</div>
			<div class="button1">
				<button name="record" type="submit"
					class="btn btn-primary btn-lg btn-block">Créer</button>
				<div class="list-group  my-1">
							<a
								class="btn btn-primary btn-lg btn-block"
								href="<%=request.getContextPath()%>/index">Annuler</a>
						</div>
			</div>
		</form>
	</div>
</body>
</html>