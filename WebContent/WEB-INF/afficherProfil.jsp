<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<jsp:include page="/WEB-INF/fragments/head.jsp"></jsp:include>
<head>
<meta charset="UTF-8">
<title>Afficher Profil</title>
</head>
<body>

	<%@ include file="./fragments/header.jsp"%>

	<h1 class="titre2">Mon profil</h1>

	<div class="container1">

		<table class="table table1">

			<tbody>
				<tr>
					<th scope="row">Pseudo</th>
					<td>${userInSession.pseudo}</td>
				</tr>
				<tr>
					<th scope="row">Nom</th>
					<td>${userInSession.nom}</td>
				</tr>
				<tr>
					<th scope="row">Prénom</th>
					<td>${userInSession.prenom}</td>
				</tr>
				<tr>
					<th scope="row">Email</th>
					<td>${userInSession.email}</td>
				</tr>
				<tr>
					<th scope="row">Téléphone</th>
					<td>${userInSession.telephone}</td>
				</tr>
				<tr>
					<th scope="row">Rue</th>
					<td>${userInSession.rue}</td>
				</tr>
				<tr>
					<th scope="row">Code Postal</th>
					<td>${userInSession.codePostal}</td>
				</tr>
				<tr>
					<th scope="row">Ville</th>
					<td>${userInSession.ville}</td>
				</tr>
			</tbody>
		</table>
		<div class="container2">
			<div class="button1">

				<a class="boutonmodifierprofil"
					href="<%=request.getContextPath()%>/modifierProfil">Modifier Profil </a>
					
			</div>
		</div>
	</div>

</body>
</html>