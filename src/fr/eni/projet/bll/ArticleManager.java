/**
 * 
 */
package fr.eni.projet.bll;

import java.util.List;

import fr.eni.projet.bo.Article;
import fr.eni.projet.dal.ArticleDAO;
import fr.eni.projet.dal.DAOFactory;
import fr.eni.projet.exception.BusinessException;


/**
 * @author Jordan
 *
 */
public class ArticleManager {
	private static ArticleDAO articleDAO;
	private static ArticleManager instance;
	
	public static ArticleManager getArticlesManager() {
		if(instance == null) {
			instance = new ArticleManager();
		}
		return instance;
	}
	
	private ArticleManager() {
		articleDAO = DAOFactory.getArticleDAO();
	}
	
	public List<Article> selectionnerArticleAccueil() throws BusinessException {
		return articleDAO.selectAll();
	}
}
