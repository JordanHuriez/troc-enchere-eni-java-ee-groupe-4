/**
 * 
 */
package fr.eni.projet.bll;

import java.sql.SQLException;


import fr.eni.projet.bo.Utilisateur;
import fr.eni.projet.dal.DAOFactory;
import fr.eni.projet.dal.UtilisateurDAO;
import fr.eni.projet.exception.BusinessException;
import fr.eni.projet.util.Constants;

/**
 * Classe en charge
 * 
 * @author mgemin2020
 * @version TrocEnchere - v1.0
 * @date 20 janv. 2021 - 11:59:30
 */
public class UtilisateurManager {

	private UtilisateurDAO utilisateurDAO;

	// Pattern Singleton
	private static UtilisateurManager instance;

	// Getter static
	public static UtilisateurManager getUtilisateurManager() {
		if (instance == null) {
			instance = new UtilisateurManager();
		}
		return instance;
	}

	private UtilisateurManager() {
		utilisateurDAO = DAOFactory.getUtilisateurDAO();
	}
	/**
	 * Methode en charge d'ajouter l'utilisateur en respectant le contrat.
	 */
	public void ajouterUtilisateur(Utilisateur u) throws BusinessException, SQLException {
		BusinessException be = new BusinessException();
//		boolean isValidPseudo = utilisateurDAO.verifierPseudo(u.getPseudo());
//		boolean isValidEmail = utilisateurDAO.verifierEmail(u.getEmail());
//		if (isValidPseudo && isValidEmail) {
			utilisateurDAO.createUser(u);
//			throw new BusinessException("Ce Pseudo est déjà pris ! sorry !");
		}

//	}
	/**
	 * Methode en charge de valider la connection d'un utilisateur.
	 */
	public Utilisateur validateConnection(String login, String password) throws BusinessException{
		BusinessException be = new BusinessException();
		boolean isValidLogin = validateLogin(login, be);
		boolean isValidPwd = validatePassword(password, be);
		if (isValidLogin && isValidPwd) {
			return utilisateurDAO.find(login, password);
		} else {
			throw be;
		}
	}
	/**
	 * Methode en charge de modifier le profil utilisateur.
	 */
	public void modifierUtilisateur(Utilisateur u) throws BusinessException, SQLException{
		try {
			validerUtilisateur(u);
			utilisateurDAO.updateUser(u);
		} catch (BusinessException be) {
			throw new BusinessException("Echec de la mise à jour utilisateur:" + u, be);
		}
	}
	
	private boolean validateLogin(String login, BusinessException be) {
		if (login == null) {
			be.addError("Pseudo est obligatoire");
			return false;
		}
		if (login.isEmpty() || login.length() < 6) {
			be.addError("Pseudo doit contenir au moins 6 caract�res");
			return false;
		}
		if (login.length() > 10) {
			be.addError("Pseudo doit contenir au plus 10 caract�res");
			return false;
		}
		return true;
	}

	private boolean validatePassword(String pwd, BusinessException be) {
		if (pwd == null) {
			be.addError("Mot de passe est obligatoire");
			return false;
		}
		if (!pwd.matches(Constants.PATTERN_PWD)) {
			be.addError("Mot de passe doit utiliser seulement des caractères alphanumériques.");
			return false;
		}
		return true;
	}

	public void validerUtilisateur(Utilisateur u) throws BusinessException {
		boolean valide = true;
		StringBuffer sb = new StringBuffer();

		if (u == null) {
			throw new BusinessException("Article null");
		}
		// Les attributs des articles sont obligatoires
		if (u.getPseudo() == null || u.getPseudo().trim().length() == 0) {
			sb.append("Le pseudo est obligatoire.\n");
			valide = false;
		}
		if (u.getNom() == null || u.getNom().trim().length() == 0) {
			sb.append("Le nom est obligatoire.\n");
			valide = false;
		}
		if (u.getPrenom() == null || u.getPrenom().trim().length() == 0) {
			sb.append("Le prénom  est obligatoire.\n");
			valide = false;
		}
		if (u.getEmail() == null || u.getEmail().trim().length() == 0) {
			sb.append("L'email est obligatoire.\n");
			valide = false;
		}
		if (u.getRue() == null || u.getRue().trim().length() == 0) {
			sb.append("Le nom de la rue est obligatoire.\n");
			valide = false;
		}
		if (u.getCodePostal() == null || u.getCodePostal().trim().length() == 0) {
			sb.append("Le code postal est obligatoire.\n");
			valide = false;
		}
		if (u.getVille() == null || u.getVille().trim().length() == 0) {
			sb.append("La ville est obligatoire.\n");
			valide = false;
		}
		if (u.getMotDePasse() == null || u.getMotDePasse().trim().length() == 0) {
			sb.append("Le mot de passe est obligatoire.\n");
			valide = false;
		}

		if (!valide) {
			throw new BusinessException(sb.toString());
		}

	}

	public boolean confirmationMDP(String motDePasse, String confirmation) {
		if (motDePasse == confirmation) {
			return true;

		} else {
			return false;
		}
	}
}
