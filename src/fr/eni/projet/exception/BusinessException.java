package fr.eni.projet.exception;

import java.util.ArrayList;
import java.util.List;

public class BusinessException extends Exception {

	private static final long serialVersionUID = -8104368022479472121L;
	//Ensemble des messages d'erreur de l'affichage
	private List<String> errors;
	
	
	public void addError(String error) {
		if(errors == null) {
			errors = new ArrayList<String>();
		}
		errors.add(error);
	}
	
	public List<String> getErrors() {
		return errors;
	}

	public BusinessException() {
		super();
	}
	
	public BusinessException(String message) {
		super(message);
	}
	
	public BusinessException(String message, Throwable exception) {
		super(message, exception);
	}

	@Override
	public String getMessage() {
		
		return "BE - " + super.getMessage();
	}
	
	

}
