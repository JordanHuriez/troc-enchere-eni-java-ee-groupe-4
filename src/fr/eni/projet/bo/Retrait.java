/**
 * 
 */
package fr.eni.projet.bo;

/**
 * @author Jordan
 *
 */
public class Retrait {
	private String rueRetrait;
	private String codePostalRetrait;
	private String villeRetrait;
	private Article articleARetirer;
	/**
	 * @return the rueRetrait
	 */
	public String getRueRetrait() {
		return rueRetrait;
	}
	/**
	 * @param rueRetrait the rueRetrait to set
	 */
	public void setRueRetrait(String rueRetrait) {
		this.rueRetrait = rueRetrait;
	}
	/**
	 * @return the codePostalRetrait
	 */
	public String getCodePostalRetrait() {
		return codePostalRetrait;
	}
	/**
	 * @param codePostalRetrait the codePostalRetrait to set
	 */
	public void setCodePostalRetrait(String codePostalRetrait) {
		this.codePostalRetrait = codePostalRetrait;
	}
	/**
	 * @return the villeRetrait
	 */
	public String getVilleRetrait() {
		return villeRetrait;
	}
	/**
	 * @param villeRetrait the villeRetrait to set
	 */
	public void setVilleRetrait(String villeRetrait) {
		this.villeRetrait = villeRetrait;
	}
	/**
	 * @return the articleARetirer
	 */
	public Article getArticleARetirer() {
		return articleARetirer;
	}
	/**
	 * @param articleARetirer the articleARetirer to set
	 */
	public void setArticleARetirer(Article articleARetirer) {
		this.articleARetirer = articleARetirer;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((articleARetirer == null) ? 0 : articleARetirer.hashCode());
		result = prime * result + ((codePostalRetrait == null) ? 0 : codePostalRetrait.hashCode());
		result = prime * result + ((rueRetrait == null) ? 0 : rueRetrait.hashCode());
		result = prime * result + ((villeRetrait == null) ? 0 : villeRetrait.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Retrait other = (Retrait) obj;
		if (articleARetirer == null) {
			if (other.articleARetirer != null)
				return false;
		} else if (!articleARetirer.equals(other.articleARetirer))
			return false;
		if (codePostalRetrait == null) {
			if (other.codePostalRetrait != null)
				return false;
		} else if (!codePostalRetrait.equals(other.codePostalRetrait))
			return false;
		if (rueRetrait == null) {
			if (other.rueRetrait != null)
				return false;
		} else if (!rueRetrait.equals(other.rueRetrait))
			return false;
		if (villeRetrait == null) {
			if (other.villeRetrait != null)
				return false;
		} else if (!villeRetrait.equals(other.villeRetrait))
			return false;
		return true;
	}
	/**
	 * @param rueRetrait
	 * @param codePostalRetrait
	 * @param villeRetrait
	 * @param articleARetirer
	 */
	public Retrait(String rueRetrait, String codePostalRetrait, String villeRetrait, Article articleARetirer) {
		super();
		this.rueRetrait = rueRetrait;
		this.codePostalRetrait = codePostalRetrait;
		this.villeRetrait = villeRetrait;
		this.articleARetirer = articleARetirer;
	}
	/**
	 * 
	 */
	public Retrait() {
		super();
	}
	
	
}
