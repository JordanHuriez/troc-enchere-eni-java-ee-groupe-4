
/**
 * 
 */
package fr.eni.projet.bo;

import java.time.LocalDate;
import java.util.List;


/**
 * @author Jordan
 *
 */
public class Article {
	private int noArticle;
	private String nomArticle;
	private String description;
	private LocalDate dateDebutEncheres;
	private LocalDate dateFinEncheres;
	private int prixInitial;
	private int prixVente;
	private Utilisateur utilisateur;
	private String etatVente;
	private Retrait lieuRetrait;
	private Categorie categorieArticle;
	private List<Enchere> listeEnchereArticle;
	/**
	 * Constructeur.
	 * @param noArticle
	 * @param nomArticle
	 * @param description
	 * @param dateDebutEncheres
	 * @param dateFinEncheres
	 * @param prixInitial
	 * @param prixVente
	 * @param utilisateur
	 * @param etatVente
	 * @param lieuRetrait
	 * @param categorieArticle
	 * @param listeEnchereArticle
	 */
	public Article(int noArticle, String nomArticle, String description, LocalDate dateDebutEncheres, LocalDate dateFinEncheres,
			int prixInitial, int prixVente, Utilisateur utilisateur, String etatVente, Retrait lieuRetrait,
			Categorie categorieArticle, List<Enchere> listeEnchereArticle) {
		super();
		this.noArticle = noArticle;
		this.nomArticle = nomArticle;
		this.description = description;
		this.dateDebutEncheres = dateDebutEncheres;
		this.dateFinEncheres = dateFinEncheres;
		this.prixInitial = prixInitial;
		this.prixVente = prixVente;
		this.utilisateur = utilisateur;
		this.etatVente = etatVente;
		this.lieuRetrait = lieuRetrait;
		this.categorieArticle = categorieArticle;
		this.listeEnchereArticle = listeEnchereArticle;
	}
	/**
	 * Constructeur.
	 */
	public Article() {
		super();
	}
	
	public Article(String nomArticle, int prix, LocalDate dateFinEncheres, Utilisateur utilisateur) {
		super();
		this.nomArticle = nomArticle;
		this.prixInitial = prix;
		this.dateFinEncheres = dateFinEncheres;
		this.utilisateur = utilisateur;
	}
	
	/**
	 * Getter pour noArticle.
	 * @return the noArticle
	 */
	public int getNoArticle() {
		return noArticle;
	}
	/**
	 * Setter pour noArticle.
	 * @param noArticle the noArticle to set
	 */
	public void setNoArticle(int noArticle) {
		this.noArticle = noArticle;
	}
	/**
	 * Getter pour nomArticle.
	 * @return the nomArticle
	 */
	public String getNomArticle() {
		return nomArticle;
	}
	/**
	 * Setter pour nomArticle.
	 * @param nomArticle the nomArticle to set
	 */
	public void setNomArticle(String nomArticle) {
		this.nomArticle = nomArticle;
	}
	/**
	 * Getter pour description.
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * Setter pour description.
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * Getter pour dateDebutEncheres.
	 * @return the dateDebutEncheres
	 */
	public LocalDate getDateDebutEncheres() {
		return dateDebutEncheres;
	}
	/**
	 * Setter pour dateDebutEncheres.
	 * @param dateDebutEncheres the dateDebutEncheres to set
	 */
	public void setDateDebutEncheres(LocalDate dateDebutEncheres) {
		this.dateDebutEncheres = dateDebutEncheres;
	}
	/**
	 * Getter pour dateFinEncheres.
	 * @return the dateFinEncheres
	 */
	public LocalDate getDateFinEncheres() {
		return dateFinEncheres;
	}
	/**
	 * Setter pour dateFinEncheres.
	 * @param dateFinEncheres the dateFinEncheres to set
	 */
	public void setDateFinEncheres(LocalDate dateFinEncheres) {
		this.dateFinEncheres = dateFinEncheres;
	}
	/**
	 * Getter pour prixInitial.
	 * @return the prixInitial
	 */
	public int getPrixInitial() {
		return prixInitial;
	}
	/**
	 * Setter pour prixInitial.
	 * @param prixInitial the prixInitial to set
	 */
	public void setPrixInitial(int prixInitial) {
		this.prixInitial = prixInitial;
	}
	/**
	 * Getter pour prixVente.
	 * @return the prixVente
	 */
	public int getPrixVente() {
		return prixVente;
	}
	/**
	 * Setter pour prixVente.
	 * @param prixVente the prixVente to set
	 */
	public void setPrixVente(int prixVente) {
		this.prixVente = prixVente;
	}
	/**
	 * Getter pour utilisateur.
	 * @return the utilisateur
	 */
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	/**
	 * Setter pour utilisateur.
	 * @param utilisateur the utilisateur to set
	 */
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	/**
	 * Getter pour etatVente.
	 * @return the etatVente
	 */
	public String getEtatVente() {
		return etatVente;
	}
	/**
	 * Setter pour etatVente.
	 * @param etatVente the etatVente to set
	 */
	public void setEtatVente(String etatVente) {
		this.etatVente = etatVente;
	}
	/**
	 * Getter pour lieuRetrait.
	 * @return the lieuRetrait
	 */
	public Retrait getLieuRetrait() {
		return lieuRetrait;
	}
	/**
	 * Setter pour lieuRetrait.
	 * @param lieuRetrait the lieuRetrait to set
	 */
	public void setLieuRetrait(Retrait lieuRetrait) {
		this.lieuRetrait = lieuRetrait;
	}
	/**
	 * Getter pour categorieArticle.
	 * @return the categorieArticle
	 */
	public Categorie getCategorieArticle() {
		return categorieArticle;
	}
	/**
	 * Setter pour categorieArticle.
	 * @param categorieArticle the categorieArticle to set
	 */
	public void setCategorieArticle(Categorie categorieArticle) {
		this.categorieArticle = categorieArticle;
	}
	/**
	 * Getter pour listeEnchereArticle.
	 * @return the listeEnchereArticle
	 */
	public List<Enchere> getListeEnchereArticle() {
		return listeEnchereArticle;
	}
	/**
	 * Setter pour listeEnchereArticle.
	 * @param listeEnchereArticle the listeEnchereArticle to set
	 */
	public void setListeEnchereArticle(List<Enchere> listeEnchereArticle) {
		this.listeEnchereArticle = listeEnchereArticle;
	}
	/**
	* {@inheritDoc}
	*/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((categorieArticle == null) ? 0 : categorieArticle.hashCode());
		result = prime * result + ((dateDebutEncheres == null) ? 0 : dateDebutEncheres.hashCode());
		result = prime * result + ((dateFinEncheres == null) ? 0 : dateFinEncheres.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((etatVente == null) ? 0 : etatVente.hashCode());
		result = prime * result + ((lieuRetrait == null) ? 0 : lieuRetrait.hashCode());
		result = prime * result + ((listeEnchereArticle == null) ? 0 : listeEnchereArticle.hashCode());
		result = prime * result + noArticle;
		result = prime * result + ((nomArticle == null) ? 0 : nomArticle.hashCode());
		result = prime * result + prixInitial;
		result = prime * result + prixVente;
		result = prime * result + ((utilisateur == null) ? 0 : utilisateur.hashCode());
		return result;
	}
	/**
	* {@inheritDoc}
	*/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Article other = (Article) obj;
		if (categorieArticle == null) {
			if (other.categorieArticle != null)
				return false;
		} else if (!categorieArticle.equals(other.categorieArticle))
			return false;
		if (dateDebutEncheres == null) {
			if (other.dateDebutEncheres != null)
				return false;
		} else if (!dateDebutEncheres.equals(other.dateDebutEncheres))
			return false;
		if (dateFinEncheres == null) {
			if (other.dateFinEncheres != null)
				return false;
		} else if (!dateFinEncheres.equals(other.dateFinEncheres))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (etatVente == null) {
			if (other.etatVente != null)
				return false;
		} else if (!etatVente.equals(other.etatVente))
			return false;
		if (lieuRetrait == null) {
			if (other.lieuRetrait != null)
				return false;
		} else if (!lieuRetrait.equals(other.lieuRetrait))
			return false;
		if (listeEnchereArticle == null) {
			if (other.listeEnchereArticle != null)
				return false;
		} else if (!listeEnchereArticle.equals(other.listeEnchereArticle))
			return false;
		if (noArticle != other.noArticle)
			return false;
		if (nomArticle == null) {
			if (other.nomArticle != null)
				return false;
		} else if (!nomArticle.equals(other.nomArticle))
			return false;
		if (prixInitial != other.prixInitial)
			return false;
		if (prixVente != other.prixVente)
			return false;
		if (utilisateur == null) {
			if (other.utilisateur != null)
				return false;
		} else if (!utilisateur.equals(other.utilisateur))
			return false;
		return true;
	}
	/**
	* {@inheritDoc}
	*/
	@Override
	public String toString() {
		return "Articles [noArticle=" + noArticle + ", nomArticle=" + nomArticle + ", description=" + description
				+ ", dateDebutEncheres=" + dateDebutEncheres + ", dateFinEncheres=" + dateFinEncheres + ", prixInitial="
				+ prixInitial + ", prixVente=" + prixVente + ", utilisateur=" + utilisateur + ", etatVente=" + etatVente
				+ ", lieuRetrait=" + lieuRetrait + ", categorieArticle=" + categorieArticle + ", listeEnchereArticle="
				+ listeEnchereArticle + "]";
	}

	
	
}