/**
 * 
 */
package fr.eni.projet.bo;

import java.util.List;

/**
 * @author Jordan
 *
 */
public class Categorie {
	private int noCategorie;
	private String libelle;
	private List<Article> listeArticles;
	/**
	 * @return the noCategorie
	 */
	public int getNoCategorie() {
		return noCategorie;
	}
	/**
	 * @param noCategorie the noCategorie to set
	 */
	public void setNoCategorie(int noCategorie) {
		this.noCategorie = noCategorie;
	}
	/**
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}
	/**
	 * @param libelle the libelle to set
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	/**
	 * @return the listeArticles
	 */
	public List<Article> getListeArticles() {
		return listeArticles;
	}
	/**
	 * @param listeArticles the listeArticles to set
	 */
	public void setListeArticles(List<Article> listeArticles) {
		this.listeArticles = listeArticles;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		result = prime * result + ((listeArticles == null) ? 0 : listeArticles.hashCode());
		result = prime * result + noCategorie;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categorie other = (Categorie) obj;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		if (listeArticles == null) {
			if (other.listeArticles != null)
				return false;
		} else if (!listeArticles.equals(other.listeArticles))
			return false;
		if (noCategorie != other.noCategorie)
			return false;
		return true;
	}
	/**
	 * @param noCategorie
	 * @param libelle
	 * @param listeArticles
	 */
	public Categorie(int noCategorie, String libelle, List<Article> listeArticles) {
		super();
		this.noCategorie = noCategorie;
		this.libelle = libelle;
		this.listeArticles = listeArticles;
	}
	/**
	 * 
	 */
	public Categorie() {
		super();
	}
	
	
}
