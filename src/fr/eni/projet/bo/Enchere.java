/**
 * 
 */
package fr.eni.projet.bo;

import java.time.LocalDate;

/**
 * @author Jordan
 *
 */
public class Enchere {
	private LocalDate dateEnchere;
	private int montantEnchere;
	private Article articleConcerne;
	private Utilisateur encherisseur;
	/**
	 * @return the dateEnchere
	 */
	public LocalDate getDateEnchere() {
		return dateEnchere;
	}
	/**
	 * @param dateEnchere the dateEnchere to set
	 */
	public void setDateEnchere(LocalDate dateEnchere) {
		this.dateEnchere = dateEnchere;
	}
	/**
	 * @return the montantEnchere
	 */
	public int getMontantEnchere() {
		return montantEnchere;
	}
	/**
	 * @param montantEnchere the montantEnchere to set
	 */
	public void setMontantEnchere(int montantEnchere) {
		this.montantEnchere = montantEnchere;
	}
	/**
	 * @return the articleConcerne
	 */
	public Article getArticleConcerne() {
		return articleConcerne;
	}
	/**
	 * @param articleConcerne the articleConcerne to set
	 */
	public void setArticleConcerne(Article articleConcerne) {
		this.articleConcerne = articleConcerne;
	}
	/**
	 * @return the encherisseur
	 */
	public Utilisateur getEncherisseur() {
		return encherisseur;
	}
	/**
	 * @param encherisseur the encherisseur to set
	 */
	public void setEncherisseur(Utilisateur encherisseur) {
		this.encherisseur = encherisseur;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((articleConcerne == null) ? 0 : articleConcerne.hashCode());
		result = prime * result + ((dateEnchere == null) ? 0 : dateEnchere.hashCode());
		result = prime * result + ((encherisseur == null) ? 0 : encherisseur.hashCode());
		result = prime * result + montantEnchere;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Enchere other = (Enchere) obj;
		if (articleConcerne == null) {
			if (other.articleConcerne != null)
				return false;
		} else if (!articleConcerne.equals(other.articleConcerne))
			return false;
		if (dateEnchere == null) {
			if (other.dateEnchere != null)
				return false;
		} else if (!dateEnchere.equals(other.dateEnchere))
			return false;
		if (encherisseur == null) {
			if (other.encherisseur != null)
				return false;
		} else if (!encherisseur.equals(other.encherisseur))
			return false;
		if (montantEnchere != other.montantEnchere)
			return false;
		return true;
	}
	/**
	 * @param dateEnchere
	 * @param montantEnchere
	 * @param articleConcerne
	 * @param encherisseur
	 */
	public Enchere(LocalDate dateEnchere, int montantEnchere, Article articleConcerne, Utilisateur encherisseur) {
		super();
		this.dateEnchere = dateEnchere;
		this.montantEnchere = montantEnchere;
		this.articleConcerne = articleConcerne;
		this.encherisseur = encherisseur;
	}
	/**
	 * 
	 */
	public Enchere() {
		super();
	}
	
	

}
