package fr.eni.projet.dal;

import fr.eni.projet.exception.BusinessException;

import java.sql.SQLException;

import fr.eni.projet.bo.Utilisateur;

public interface UtilisateurDAO {
	Utilisateur find(String login, String password)  throws BusinessException ;
	public void createUser(Utilisateur u) throws BusinessException, SQLException;
	public Boolean verifierPseudo(String pseudo) throws BusinessException ;
	public Boolean verifierEmail(String email) throws BusinessException ;
	public void updateUser(Utilisateur u) throws BusinessException, SQLException;
}
