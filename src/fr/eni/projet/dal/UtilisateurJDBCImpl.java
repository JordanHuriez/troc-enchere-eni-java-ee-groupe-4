/**
 * 
 */
package fr.eni.projet.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import fr.eni.projet.bo.Utilisateur;
import fr.eni.projet.exception.BusinessException;
import jdk.javadoc.internal.doclets.toolkit.taglets.ThrowsTaglet;

/**
 * Classe en charge
 * 
 * @author mgemin2020
 * @version TrocEnchere - v1.0
 * @date 20 janv. 2021 - 12:14:43
 */
public class UtilisateurJDBCImpl implements UtilisateurDAO {

	private static final String CONNECTION = "SELECT * from utilisateurs where pseudo = ? and mot_de_passe = ?";
	private static final String INSERT_UTILISATEUR = "INSERT INTO UTILISATEURS (pseudo, nom, prenom, email, telephone, rue, code_postal, ville, mot_de_passe, credit, administrateur)values (?,?,?,?,?,?,?,?,?,?,?)";
	private static final String SEARCH_PSEUDO = "SELECT pseudo FROM UTILISATEURS WHERE pseudo like '\"+pseudo+\"'";
	private static final String SEARCH_EMAIL = "SELECT email FROM UTILISATEURS WHERE email like '\"+email+\"'";
	private static final String UPDATE = "UPDATE UTILISATEURS SET pseudo=?,nom=?,prenom=?,email=?,telephone=?,rue=?,code_postal=?,ville=?,mot_de_passe=? WHERE no_utilisateur = ?";

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unused")
	public void updateUser(Utilisateur u) throws BusinessException, SQLException {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement rqt = cnx.prepareStatement(UPDATE);

			rqt.setString(1, u.getPseudo());
			rqt.setString(2, u.getNom());
			rqt.setString(3, u.getPrenom());
			rqt.setString(4, u.getEmail());
			rqt.setString(5, u.getTelephone());
			rqt.setString(6, u.getRue());
			rqt.setString(7, u.getCodePostal());
			rqt.setString(8, u.getVille());
			rqt.setString(9, u.getMotDePasse());
			
			rqt.setInt(10, u.getNoUtilisateur());

			rqt.executeUpdate();

		} catch (SQLException e) {
			throw new BusinessException("Update utilisateur failed - " + u, e);
		}

	}
	/**
	 * Methode en charge de vérifier le format du pseudo.
	 */
	public Boolean verifierPseudo(String pseudo) throws BusinessException {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SEARCH_PSEUDO);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				String resultat;
				resultat = (rs.getString("pseudo"));
				if (resultat.matches("[A-Za-z0-9]+")) {
					return true;
				} else {
					return false;
				}
			}
			if (rs != null) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.addError("Error DB");
			throw be;
		}
	}
	/**
	 * Methode en charge de verifier si un email est déjà present ou non dans la base de donnée.
	 */
	public Boolean verifierEmail(String email) throws BusinessException {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SEARCH_EMAIL);
			ResultSet rs = pstmt.executeQuery();
			if (rs != null) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.addError("Error DB");
			throw be;
		}
	}

	@Override
	public Utilisateur find(String login, String password) throws BusinessException {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(CONNECTION);
			pstmt.setString(1, login);
			pstmt.setString(2, password);

			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				Utilisateur u = new Utilisateur();
				u.setPseudo(rs.getString("pseudo"));
				u.setMotDePasse(rs.getString("mot_de_passe"));
				u.setNom(rs.getString("nom"));
				u.setPrenom(rs.getString("prenom"));
				u.setEmail(rs.getString("email"));
				u.setTelephone(rs.getString("telephone"));
				u.setRue(rs.getString("rue"));
				u.setCodePostal(rs.getString("code_postal"));
				u.setVille(rs.getString("ville"));
				u.setCredit(rs.getInt("credit"));
				u.setNoUtilisateur(rs.getInt("no_utilisateur"));

				return u;
			} else {
				BusinessException be = new BusinessException();
				be.addError("Pseudo ou Mot de passe inconnu");
				throw be;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.addError("Error DB");
			throw be;
		}
	}
	/**
	 * Methode en charge de créer un utilisateur et de l'ajouter a la base de donnée.
	 */
	public void createUser(Utilisateur u) throws BusinessException, SQLException {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement rqt = cnx.prepareStatement(INSERT_UTILISATEUR, Statement.RETURN_GENERATED_KEYS);

			rqt.setString(1, u.getPseudo());
			rqt.setString(2, u.getNom());
			rqt.setString(3, u.getPrenom());
			rqt.setString(4, u.getEmail());
			rqt.setString(5, u.getTelephone());
			rqt.setString(6, u.getRue());
			rqt.setString(7, u.getCodePostal());
			rqt.setString(8, u.getVille());
			rqt.setString(9, u.getMotDePasse());
			rqt.setInt(10, 100);
			rqt.setInt(11, 0);
			rqt.executeUpdate();

			try (ResultSet generatedKeys = rqt.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					u.setNoUtilisateur(generatedKeys.getInt(1));

				} else {
					throw new SQLException("Impossible de créer l'utilisateur, aucun ID obtenu.");
				}

			} catch (SQLException e) {
				throw new BusinessException("Insert utilisateur failed");
			} finally {
				try {
					if (rqt != null)
						rqt.close();
					if (cnx != null)
						cnx.close();
				} catch (SQLException e) {
					throw new BusinessException("Probleme - fermerConnexion - " + e.getMessage());
				}
			}

		}

	}
}
