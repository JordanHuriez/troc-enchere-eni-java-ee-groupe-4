package fr.eni.projet.dal;

import java.util.List;

import fr.eni.projet.bo.Article;
import fr.eni.projet.exception.BusinessException;


public interface ArticleDAO {
	public List<Article> selectAll() throws BusinessException;
	public Article selectByCategorie(int no_categorie) throws BusinessException;

}
