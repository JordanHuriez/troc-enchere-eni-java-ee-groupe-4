package fr.eni.projet.dal;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionProvider {
	private static DataSource dataSource;

	/**
	 * Au chargement de la classe, la DataSource 
	 * est recherchée dans l'arbre JNDI
	 */
	static {
		Context context;
		try{
			context = new InitialContext();
			ConnectionProvider.dataSource = 
					(DataSource) context.lookup("java:comp/env/jdbc/pool_cnx_encheres");
		}catch (NamingException e) {
			e.printStackTrace();
			throw new RuntimeException("Cannot access the database");
		}
	}
	
	/**
	 * Getter pour acc�der � la connexion
	 */
	public static Connection getConnection()throws SQLException{
		return ConnectionProvider.dataSource.getConnection();
	}

}
