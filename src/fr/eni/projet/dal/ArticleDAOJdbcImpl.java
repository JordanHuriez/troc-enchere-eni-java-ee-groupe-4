package fr.eni.projet.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import fr.eni.projet.bo.Article;
import fr.eni.projet.bo.Utilisateur;
import fr.eni.projet.exception.BusinessException;
import fr.eni.projet.util.Errors;

public class ArticleDAOJdbcImpl implements ArticleDAO {

	private static final String SELECT_BY_CATEGORIE = "SELECT * FROM ARTICLES where no_categorie = ?";
	private static final String SELECT_ACCUEIL = "SELECT nom_article, prix_vente, date_fin_encheres, pseudo, a.no_utilisateur FROM ARTICLES AS a LEFT JOIN UTILISATEURS AS u ON a.no_utilisateur = u.no_utilisateur";
/**
 * Methode en charge d'afficher la listes de tout les articles en ventes.
 */
	public List<Article> selectAll() throws BusinessException {
		List<Article> lstarticles = new ArrayList<Article>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			Statement pstmt = cnx.createStatement();
			ResultSet rs = pstmt.executeQuery(SELECT_ACCUEIL);
			while (rs.next()) {
				int noUtilisateur = rs.getInt("no_utilisateur");
				String pseudo = rs.getString("pseudo");
				Utilisateur user = new Utilisateur(noUtilisateur, pseudo);
				java.util.Date dateFinEnchere = rs.getDate("date_fin_encheres");
				Article a = new Article(rs.getString("nom_article"), 
						rs.getInt("prix_vente"), rs.getDate("date_fin_encheres").toLocalDate(), user);
				lstarticles.add(a);}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstarticles;
	}
	/**
	 * Methode en charge d'afficher les articles par categorie.
	 */
	@Override
	public Article selectByCategorie(int no_categorie) throws BusinessException {
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_BY_CATEGORIE);
			pstmt.setInt(1, no_categorie);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				Article article = new Article();
				return article;
			} else {
				BusinessException businessException = new BusinessException();
				businessException.addError(Errors.LECTURE_ARTICLES_ECHEC);
				throw businessException;
			}
		} catch (Exception e) {
			e.printStackTrace();
			BusinessException businessException = new BusinessException();
			businessException.addError(Errors.LECTURE_ARTICLES_ECHEC);
			throw businessException;
		}
	}
	
}
