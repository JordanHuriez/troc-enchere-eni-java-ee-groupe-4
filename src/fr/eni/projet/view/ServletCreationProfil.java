package fr.eni.projet.view;

import java.io.IOException;
import java.sql.SQLException;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.projet.bll.UtilisateurManager;
import fr.eni.projet.bo.Utilisateur;
import fr.eni.projet.exception.BusinessException;

/**
 * Servlet implementation class ServletModifierProfil
 */
@WebServlet("/creationProfil")
public class ServletCreationProfil extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletCreationProfil() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/creationProfil.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		
		String pseudo = request.getParameter("pseudo");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String email = request.getParameter("email");
		String telephone = request.getParameter("telephone");
		String rue = request.getParameter("rue");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		String motDePasse = request.getParameter("motDePasse");
		String confirmation = request.getParameter("confirmation");
		
		Utilisateur u = new Utilisateur(pseudo, nom, prenom, email, telephone, rue, codePostal, ville, motDePasse);
		u.setPseudo(pseudo);
		u.setNom(nom);
		u.setPrenom(prenom);
		u.setEmail(email);
		u.setTelephone(telephone);
		u.setRue(rue);
		u.setCodePostal(codePostal);
		u.setVille(ville);
		u.setMotDePasse(motDePasse);
		
		System.out.println(u);

		if (request.getParameter("motDePasse").equals(request.getParameter("confirmation"))) {
			try {
				UtilisateurManager.getUtilisateurManager().ajouterUtilisateur(u);
			} catch (BusinessException | SQLException e) {
				e.printStackTrace();
			}
			request.setAttribute("messageCreation", u.getCredit());
			
			HttpSession session  = request.getSession();
			session.setAttribute("userInSession", u);
			request.setAttribute("user", u);
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/indexConnected.jsp").forward(request, response);
		} else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/CreationProfil.jsp").forward(request, response);
			
		}
	}
}


