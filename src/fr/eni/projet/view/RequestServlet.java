package fr.eni.projet.view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RequestServlet
 */
@WebServlet("/RequestServlet")
public class RequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//Gestion de l'encodage des caracteres
		request.setCharacterEncoding("UTF-8");
		
		// Recuper les parametres du formulaire
		String recherche = request.getParameter("recherche");
		String submit = request.getParameter("submit");
		System.out.println("Recherche : " + recherche);
	}

}
