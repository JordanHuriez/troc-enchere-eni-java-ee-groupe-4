package fr.eni.projet.view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.projet.bll.UtilisateurManager;
import fr.eni.projet.bo.Utilisateur;
import fr.eni.projet.exception.BusinessException;

/**
 * Servlet implementation class SessionServlet
 */
@WebServlet("/connection")
public class ServletConnection extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletConnection() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String identifiant = request.getParameter("login");
		String password = request.getParameter("password");
		
		try {
			Utilisateur u = UtilisateurManager.getUtilisateurManager().validateConnection(identifiant, password);
			System.out.println(identifiant + password);
			System.out.println(u.toString());
			HttpSession session  = request.getSession();
			session.setAttribute("userInSession", u);
			request.setAttribute("user", u);
			request.getRequestDispatcher("/WEB-INF/indexConnected.jsp").forward(request, response);
		} catch (BusinessException e) {
			request.setAttribute("errors", e.getErrors());
		    request.getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
		}
		
	}

}
