package fr.eni.projet.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.projet.bll.ArticleManager;
import fr.eni.projet.bo.Article;
import fr.eni.projet.exception.BusinessException;

/**
 * Servlet implementation class ServletIndex
 */
@WebServlet("/index")
public class ServletIndex extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletIndex() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			List<Article> lstArticles = ArticleManager.getArticlesManager().selectionnerArticleAccueil();
			request.setAttribute("lstArticles", lstArticles);
			request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);		
		} catch (BusinessException e) {
			e.printStackTrace();
			//page de gestion d'erreur.
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Gestion de l'encodage des caracteres
		request.setCharacterEncoding("UTF-8");
		
		// Récuper les parametres du formulaire
		String recherche = request.getParameter("recherche");
		String submit = request.getParameter("submit");
		String categorie = request.getParameter("categorie");
		System.out.println("Recherche : " + recherche);
		System.out.println("Categorie : " + categorie);
		//Passer dans le contexte de requete
		request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);	
	}

}
