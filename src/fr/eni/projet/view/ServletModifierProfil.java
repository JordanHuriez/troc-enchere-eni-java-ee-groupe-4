package fr.eni.projet.view;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import fr.eni.projet.bll.UtilisateurManager;
import fr.eni.projet.bo.Utilisateur;
import fr.eni.projet.exception.BusinessException;

/**
 * Servlet implementation class ServletModifierProfil
 */
@WebServlet("/modifierProfil")
public class ServletModifierProfil extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletModifierProfil() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/modifierProfil.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String pseudo = request.getParameter("pseudo");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String email = request.getParameter("email");
		String telephone = request.getParameter("telephone");
		String rue = request.getParameter("rue");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		String motDePasse = request.getParameter("nouveauMotDePasse");
		
		HttpSession session  = request.getSession();
		
		Utilisateur u = (Utilisateur) session.getAttribute("userInSession");
		
		
		u.setPseudo(pseudo);
		u.setNom(nom);
		u.setPrenom(prenom);
		u.setEmail(email);
		u.setTelephone(telephone);
		u.setRue(rue);
		u.setCodePostal(codePostal);
		u.setVille(ville);
		u.setMotDePasse(motDePasse);

		
		System.out.println(u);

		if (request.getParameter("nouveauMotDePasse").equals(request.getParameter("confirmation"))) {
			try {
				UtilisateurManager.getUtilisateurManager().modifierUtilisateur(u);
				
				this.getServletContext().getRequestDispatcher("/WEB-INF/modifierProfil.jsp").forward(request, response);
			} catch (BusinessException | SQLException e) {
				e.printStackTrace();
			}
			request.setAttribute("Votre profil a été modifié", u.getPseudo());
			
			
		}
	}	
}
